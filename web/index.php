<?php
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

chdir(dirname(__DIR__));

require_once "vendor/autoload.php";

$app = new Application();

require_once "app/bootstrap.php";

$app->before(function(Request $req) use ($app){
	$route = substr($req->getPathInfo(), 8);
	if (in_array($route, $app["controlled.routes"])) {
		$jwt = $req->headers->get("x-access-token");
		try {
			$decoded = $app["jwt.decode"]($jwt);
		} catch (\Exception $e) {
			return new JsonResponse(array("error" => $e->getMessage()), 401);
		}
	}
});

$app->run();