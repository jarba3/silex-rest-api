<?php
use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Igorw\Silex\ConfigServiceProvider;
use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use MJanssen\Provider\ServiceRegisterProvider;

$app->register(new ServiceControllerServiceProvider());

$app->register(new ConfigServiceProvider(__DIR__."/config/services.php"));
$app->register(new ServiceRegisterProvider());
$app->register(new ConfigServiceProvider(__DIR__."/config/config.php"));
$app->register(new DoctrineServiceProvider());
$app->register(new DoctrineOrmServiceProvider());
$app->register(new ValidatorServiceProvider());

require_once "config/routes.php";