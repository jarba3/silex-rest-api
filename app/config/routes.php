<?php

$routes = array(
		new UserBundle\Routes\AuthRoutes($app),
	);

foreach ($routes as $route) {
	$route->bindRoutesToControllers();
}