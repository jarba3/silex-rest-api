<?php
return array(
		"config.providers" => array(
				"data.services" =>array(
						"class" => "Common\Services\ValidationService"
					),
				"jwt.services" => array(
						"class" => "Common\services\JwtService"
					)
			)
	);