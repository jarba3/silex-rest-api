<?php
return array(
	"jwt.alg" => "RS256",
	"jwt.exp" => time()+86400,
	"jwt.priv.key" => file_get_contents(__DIR__."/../jwtKey/privkey.pem"),
	"jwt.pub.key" => file_get_contents(__DIR__."/../jwtKey/pubkey.pem"),
	"api.endpoint" => "api",
	"api.version" => "v1",
	"db.options" => array(
			"driver" => "pdo_mysql",
			"charset" => "UTF8",
			"user" => "root",
			"password" => "",
			"host" => "localhost",
			"dbname" => "silex_rest",
		),
	"orm.proxies_dir" => __DIR__."/../cache/doctrine",
	"orm.em.options" => array(
			"mappings" => array(
					array(
							"type" => "annotation",
							"alias" => "UserBundle",
							"namespace" => "UserBundle\Entity",
							"path" => __DIR__."/../../src/UserBundle/Entity",
							"use_simple_annotation_reader" => false
						),
				)
		),
	"controlled.routes" => array(
		"login"
		)
	);