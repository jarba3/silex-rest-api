<?php
namespace UserBundle\Routes;

use Silex\Application;
use UserBundle\Controllers\AuthController;

class AuthRoutes
{
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->instantiateControllers();
    }

    private function instantiateControllers()
    {
        $this->app['auth.controller'] = $this->app->share(function () {
            return new AuthController($this->app);
        });
    }
    
    public function bindRoutesToControllers()
    {
        $api = $this->app["controllers_factory"];

        $api->post("/login", "auth.controller:login");
        $api->post("/register", "auth.controller:register");
        $api->post("/logout", "auth.controller:logout");

        $this->app->mount($this->app["api.endpoint"].'/'.$this->app["api.version"], $api);
    }
}