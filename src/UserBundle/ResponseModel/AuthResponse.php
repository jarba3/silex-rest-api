<?php
namespace UserBundle\ResponseModel;

class AuthResponse
{
	public function response($obj, $jwt)
	{
		if (!$obj) {
			return null;
		}else{
			return array(
					"id" => $obj->getId(),
					"email" => $obj->getEmail(),
					"firstname" => $obj->getFirstname(),
					"lastname" => $obj->getLastname(),
					"x-access-token" => $jwt
				);
		}
	}
}