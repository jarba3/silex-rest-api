<?php
namespace UserBundle\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Firebase\JWT\JWT;
use UserBundle\Entity\User;
use UserBundle\ResponseModel\AuthResponse;

class AuthController
{
	private $app;

	public function __construct(Application $app)
	{
		$this->app = $app;
		$this->em = $app["orm.em"];
	}

	public function login(Request $req)
	{
		$data = json_decode($req->getContent());

		try {
			$notBlank = $this->app["notBlank.validator"]($data);
		} catch (\Exception $e) {
			return new JsonResponse(array("error" => $e->getMessage()), 400);
		}
		
		$user = $this->em->getRepository("UserBundle:User")->findOneByEmail($data->email);
		if (!$user || !$user->verifyPassword($data->password)) {
			return new JsonResponse(array("error" => "The email or/and password is incorrect"), 400);
		}
		$jwt = $this->app["jwt.encode"]($user);

		return new JsonResponse((new AuthResponse())->response($user, $jwt), 200);
	}

	public function register(Request $req)
	{
		$data = json_decode($req->getContent());

		try {
			$notBlank = $this->app["notBlank.validator"]($data);
			$email = $this->app["email.validator"]($data);
			$password = $this->app["string.validator"]($data, "password");
			$firstname = $this->app["string.validator"]($data, "firstname");
			$lastname = $this->app["string.validator"]($data, "lastname");
			$exist = $this->app["already.exist.validator"]("UserBundle:User", "email", $email);
		} catch (\Exception $e) {
			return new JsonResponse(array("error" => $e->getMessage()), 400);
		}

		$user = new User();

		$user->setEmail($email)
			 ->setPassword($password)
			 ->setLastname($lastname)
			 ->setFirstname($firstname);

		$this->em->persist($user);
		try {
			$this->em->flush();
		} catch (\Exception $e) {
			return new JsonResponse(array("error" => "Impossible to persist data"), 400);
		}
		$jwt = $this->app["jwt.encode"]($user);

		return new JsonResponse((new AuthResponse())->response($user, $jwt), 200);
	}

	public function logout(Request $req)
	{
		return new Response("", 204);
	}
}