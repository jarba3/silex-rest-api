<?php
namespace UserBundle\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProfileController
{
	private $app;

	function __construct($app)
	{
		$this->app = $app;
		$this->em = $app["orm.em"];
	}

	public function getProfile()
	{
		
	}

	public function putProfile(Request $req)
	{
		
	}

	public function deleteProfile()
	{
		
	}
}