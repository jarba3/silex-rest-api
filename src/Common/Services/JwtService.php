<?php
namespace Common\Services;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Firebase\JWT\JWT;

class JwtService implements ServiceProviderInterface
{
	public function register(Application $app)
	{
		$app["jwt.encode"] = $app->protect(function($user) use ($app){
			$payload = array(
				"aud" => $user->getEmail(),
				"exp" => $app["jwt.exp"],
				);
			$jwt = JWT::encode($payload, $app["jwt.priv.key"], $app["jwt.alg"]);
			return $jwt;
		});

		$app["jwt.decode"] = $app->protect(function($jwt) use ($app){
			return JWT::decode($jwt, $app["jwt.pub.key"], array($app["jwt.alg"]));
		});
	}

	public function boot(Application $app)
	{
	}

}