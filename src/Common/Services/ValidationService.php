<?php
namespace Common\Services;

use Silex\Application;
use Silex\ServiceProviderInterface;

class ValidationService implements ServiceProviderInterface
{
	public function register(Application $app)
	{
		$app["notBlank.validator"] = $app->protect(function($data) use ($app){
			/**
			 * For all key in data we check if value is not blank
			 * @var std object
			 */
			foreach ($data as $key => $value) {
				if (!preg_match("/[\s\S]/", $value)) {
					if (!is_bool($value)) {
						throw new \Exception("The $key must be not null");						
					}
				}
			}
		});

		$app["email.validator"] = $app->protect(function($data) use ($app){
			if (isset($data->email)) {
				if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
					throw new \Exception("This email is not valid");					
				}
			}else{
				throw new \Exception("The email must be filled");				
			}
			return filter_var($data->email, FILTER_SANITIZE_EMAIL);
		});

		$app["string.validator"] = $app->protect(function($data, $fieldName) use ($app){
			if (!isset($data->$fieldName)) {
				throw new \Exception("The $fieldName must be filled");				
			}
			return filter_var($data->$fieldName, FILTER_SANITIZE_STRING);
		});

		$app["already.exist.validator"] = $app->protect(function($repo, $fieldName, $value) use ($app){
			$em = $app["orm.em"];
			$exist = $em->getRepository($repo)->findBy(array($fieldName => $value));
			if ($exist) {
				throw new \Exception("The $fieldName already exist");
			}
		});
	}

	public function boot(Application $app)
	{
	}

}